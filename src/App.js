import React from "react";
import "./App.css";
import TodoApp from "./TodoApp";

const App = () => {
  return (
    <div>
      <h1>JS</h1>
      <TodoApp />
    </div>
  );
};

export default App;
