function appReducer(state, action) {
  switch (action.type) {
    case "ADD_TODO":
      return [
        ...state,
        {
          id: Date.now(),
          text: action.payload,
          completed: false
        }
      ];
    case "DELETE_TODO":
      return state.filter(item => item.id !== action.payload);
    case "COMPLETE_TODO":
      return state.map((item, i) => {
        if (item.id === action.payload) {
          return { ...item, completed: !item.completed };
        }
        return item;
      });
    case 'RESET':
      return action.payload
    default:
      return state;
  }
}

export default appReducer;