import React, { useReducer, useEffect, useRef } from "react";
import TodosList from "./TodosList";
import appReducer from "./App.Reducer";

export const Context = React.createContext();

function useEffectOnce(cb) {
  const didRun = useRef(false);

  useEffect(() => {
    if (!didRun.current) {
      cb();

      didRun.current = true;
    }
    console.log("from useEffect");
  }, []);
}

const TodoApp = () => {
  const [state, dispatch] = useReducer(appReducer, []);

  useEffectOnce(() => {
    const raw = localStorage.getItem("data");
    dispatch({ type: "RESET", payload: JSON.parse(raw) });
  });

  useEffect(
    () => {
      localStorage.setItem("data", JSON.stringify(state));
    },
    [state]
  );

  return (
    <Context.Provider value={dispatch}>
      <h1>Todo App</h1>
      <button
        onClick={() => dispatch({ type: "ADD_TODO", payload: "Hooks JS" })}
      >
        Add New Todo
      </button>
      <TodosList items={state} dispatch={dispatch} />
    </Context.Provider>
  );
};

export default TodoApp;
