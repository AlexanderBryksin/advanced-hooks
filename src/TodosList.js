import React from "react";
import TodoItem from "./TodoItem";

const TodosList = ({ items }) => {
  return (
    <div>
      {items.map((item, i) => (
        <TodoItem key={item.id} {...item} />
      ))}
    </div>
  );
};

export default TodosList;
