import React, { useContext } from "react";
import { Context } from "./TodoApp";

const TodoItem = ({ id, text, completed }) => {
  const dispatch = useContext(Context);

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between"
      }}
    >
      <input
        type="checkbox"
        checked={completed}
        onChange={() => dispatch({ type: "COMPLETE_TODO", payload: id })}
      />
      <input type="text" defaultValue={text} />
      <button onClick={() => dispatch({ type: "DELETE_TODO", payload: id })}>
        Delete
      </button>
      id: {id} text: {text}
    </div>
  );
};

export default TodoItem;

